from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.shortcuts import render, redirect
from accounts.forms import LogInForm, SignUpForm


# Create your views here.
def user_login(request):
    if request.method == "POST":
        # Get the form data from the request
        form = LogInForm(request.POST)
        if form.is_valid():
            # Get the username and password from the form
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]

            # Try to authenticate the user
            user = authenticate(request, username=username, password=password)
            if user is not None:
                # If authentication was successful, log the user in and
                # redirect to the home page
                login(request, user)
                return redirect("home")
    else:
        # If this is a GET request, create a new form instance
        form = LogInForm()
    context = {
        "form": form,
    }
    # Render the log in page with the form
    return render(request, "accounts/login.html", context)


def user_logout(request):
    logout(request)
    return redirect("login")


def user_signup(request):
    if request.method == "POST":
        # create new SignUpForm object with request.POST data
        form = SignUpForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            confirm = form.cleaned_data["password_confirmation"]
            if password == confirm:
                # Create a new user object with username and password
                user = User.objects.create_user(username, password=password)
                # Log the user in
                login(request, user)
                # Redirect to the "home" page
                return redirect("home")
            else:
                # Add an error message to the password field in the form
                form.add_error("password", "the passwords do not match")
    else:
        # Create a SignUpForm instance with no data
        form = SignUpForm()
    context = {
        "form": form
    }
    # Render the html with the request and context
    return render(request, "accounts/signup.html", context)
